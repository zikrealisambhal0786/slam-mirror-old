import os
CMD_NUM = os.environ.get("CMD_NUM", '')

class _BotCommands:
    def __init__(self):
        self.StartCommand = 'start'
        self.MirrorCommand = f'mirror{CMD_NUM}'
        self.UnzipMirrorCommand = f'unzipmirror{CMD_NUM}'
        self.TarMirrorCommand = f'tarmirror{CMD_NUM}'
        self.CancelMirror = f'cancel{CMD_NUM}'
        self.CancelAllCommand = f'cancelall{CMD_NUM}'
        self.ListCommand = f'list{CMD_NUM}'
        self.StatusCommand = f'status{CMD_NUM}'
        self.AuthorizedUsersCommand = 'users'
        self.AuthorizeCommand = 'authorize'
        self.UnAuthorizeCommand = 'unauthorize'
        self.AddSudoCommand = 'addsudo'
        self.RmSudoCommand = 'rmsudo'
        self.PingCommand = 'ping'
        self.RestartCommand = f'restart{CMD_NUM}'
        self.StatsCommand = f'stats{CMD_NUM}'
        self.HelpCommand = f'help{CMD_NUM}'
        self.LogCommand = 'log'
        self.SpeedCommand = 'speedtest'
        self.CloneCommand = f'clone{CMD_NUM}'
        self.CountCommand = f'count{CMD_NUM}'
        self.WatchCommand = f'watch{CMD_NUM}'
        self.TarWatchCommand = f'tarwatch{CMD_NUM}'
        self.DeleteCommand = f'del{CMD_NUM}'
        self.UsageCommand = 'usage'
        self.MediaInfoCommand = f'media_info{CMD_NUM}'
        self.ConfigMenuCommand = 'config'
        self.ShellCommand = 'shell'
        self.UpdateCommand = 'update'
        self.ExecHelpCommand = 'exechelp'
        self.TsHelpCommand = f'tshelp{CMD_NUM}'
        self.GDTOTCommand = f'gdtot{CMD_NUM}'

BotCommands = _BotCommands()
